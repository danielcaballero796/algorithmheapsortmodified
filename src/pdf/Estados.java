package pdf;

import com.itextpdf.text.BaseColor;

/**
 *
 * @author danis
 */
public enum Estados {
    
    INACTIVO(new BaseColor(255, 255, 255)),
    ACTIVO(new BaseColor(230, 173, 181)),
    MARCADO(new BaseColor(210, 224, 158));

    private BaseColor color;

    private Estados(BaseColor color) {
        this.color = color;
    }

    public BaseColor getColor() {
        return color;
    }

    public void setColor(BaseColor color) {
        this.color = color;
    }

}
