/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author danis
 */
public class GenerarPdf<T> {

    private Secuencia<T> result;
    private Secuencia<T> vector;
    private final Secuencia<Celda> celdas;
    Document document = new Document();

    public GenerarPdf(Secuencia<T> vtc) throws DocumentException {
        this.celdas = new Secuencia<>(vtc.getTamanio());
        this.vector = new Secuencia(vtc.getTamanio());
        Integer x = 0;
        this.add((T) x);
        for (int i = 1; i < vtc.getTamanio(); i++) {
            this.add(vtc.get(i));
        }
        llenarCeldas();
    }

    private void llenarCeldas() {
        Celda c;

        for (int i = 0; i < vector.getTamanio(); i++) {
            c = new Celda((Integer) vector.get(i), 0, 0);
            celdas.insertar(c);
        }

    }

    public void add(T info) throws DocumentException {
        this.vector.insertar(info);
        siftUp();
    }

    public void siftUp() throws DocumentException {
        int son = this.vector.getTamanio() - 1;
        while (son > 1) {
            int dad = son / 2;
            if ((Integer) this.vector.get(dad) < (Integer) this.vector.get(son)) {
                return;
            } else {
                swap(dad, son);
                son = dad;
            }

        }
    }

    public void swap(int dad, int son) throws DocumentException {
        T temp = this.vector.get(dad);
        this.vector.set(dad, this.vector.get(son));
        this.vector.set(son, temp);
    }

    public T deleteHeap() throws DocumentException {
        T res = this.vector.get(1);
        this.vector.set(1, this.vector.get(this.vector.getTamanio() - 1));
        this.vector.setCant(this.vector.getTamanio() - 1);
        this.siftDown();
        return res;

    }

    public void siftDown() throws DocumentException {
        int dad = 1;
        while (dad <= this.vector.getTamanio() / 2) {
            int son = dad * 2;
            if (son > this.vector.getTamanio()) {
                break;
            }
            if (son + 1 <= this.vector.getTamanio() && (Integer) (this.vector.get(son + 1)) < (Integer) (this.vector.get(son))) {
                son++;
            }
            if ((Integer) (this.vector.get(dad)) < (Integer) (this.vector.get(son))) {
                break;
            }

            swap(dad, son);
            dad = son;

        }
    }

    public Secuencia<T> getVector() {
        return vector;
    }

    public String generar() {

        try {
            File f = new File("HeapSort.pdf");

            try {
                PdfWriter.getInstance(this.document, new FileOutputStream(f));
            } catch (FileNotFoundException fileNotFoundException) {
                System.out.println("(No se encontró el fichero para generar el pdf)" + fileNotFoundException);
            }

            document.addTitle("Metodos de ordenamiento Heap Sort");
            document.addSubject("PDF con los pasos del metodo de ordenamiento Heap Sort");
            document.addAuthor("Paola Pajaro, Daniel Caballero y Jose Jaimes");
            document.addCreator("Paola Pajaro, Daniel Caballero y Jose Jaimes estudiantes de la UFPS");
            document.open();

            document.add(new Paragraph("PDF con los pasos del metodo de ordenamiento Heap Sort", FontFactory.getFont("arial", 18, Font.BOLD, BaseColor.BLACK)));
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Estudiantes: \nPaola Pajaro: 1151380 \nDaniel Caballero: 1151267 \nJose Jaimes: 1151693", FontFactory.getFont("arial", 14, Font.BOLD, BaseColor.BLACK)));
            document.add(new Paragraph("Repositorio de Gitlab: https://gitlab.com/danielcaballero796/algorithmheapsort", FontFactory.getFont("arial", 12, Font.BOLD, BaseColor.BLACK)));

            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Vector Inicial: " + this.vector.toString(), FontFactory.getFont("arial", 12, Font.NORMAL, BaseColor.RED)));
            document.add(new Paragraph("\n"));

            int cont = 0;
            Secuencia x = new Secuencia(this.vector.getTamanio());
            for (int i = this.vector.getTamanio() - 1; i > 0; i--) {
                cont++;
                T res = this.vector.get(1);
                this.vector.set(1, this.vector.get(this.vector.getTamanio() - 1));
                this.vector.setCant(this.vector.getTamanio() - 1);

                int dad = 1;
                while (dad <= this.vector.getTamanio() / 2) {
                    int son = dad * 2;
                    if (son > this.vector.getTamanio()) {
                        break;
                    }
                    if (son + 1 <= this.vector.getTamanio() && (Integer) (this.vector.get(son + 1)) < (Integer) (this.vector.get(son))) {
                        son++;
                    }
                    if ((Integer) (this.vector.get(dad)) < (Integer) (this.vector.get(son))) {
                        break;
                    }

                    T temp = this.vector.get(dad);
                    this.vector.set(dad, this.vector.get(son));
                    this.vector.set(son, temp);
                    document.add(new Paragraph("Operaciones para la insercion: Cambio de posicion entre " + this.vector.get(dad) + " y " + this.vector.get(son), FontFactory.getFont("arial", 12, Font.NORMAL, BaseColor.BLACK)));
                    dad = son;

                }

                document.add(new Paragraph("\n"));
                document.add(new Paragraph("Iteracion " + cont + ": el valor ordenado a insertar es " + res, FontFactory.getFont("arial", 14, Font.BOLD, BaseColor.BLACK)));
                document.add(new Paragraph("\n"));

                if (i != 1) {
                    document.add(new Paragraph(this.vector.toString()));
                }

                document.add(new Paragraph("\n"));
                x.insertar(res);

            }

            this.vector = x;
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Vector Ordenado a traves de HeapSort: " + this.vector.toString(), FontFactory.getFont("arial", 14, Font.NORMAL, BaseColor.RED)));
            document.close();

            System.out.println("¡Se ha generado el PDF!");
            return f.getAbsolutePath();
        } catch (DocumentException documentException) {
            System.out.println("Se ha producido un error al generar un documento: " + documentException);
        }

        return null;
    }

//    public static void main(String[] args) throws DocumentException {
//        Secuencia x = new Secuencia(10);
//        x.insertar(0);
//        x.insertar(65);
//        x.insertar(4);
//        x.insertar(7);
//        x.insertar(9);
//        x.insertar(10);
//        x.insertar(34);
//        x.insertar(62);
//        x.insertar(66);
//        x.insertar(68);
//
//        GenerarPdf prueba = new GenerarPdf(x);
//        prueba.generar();
//
//        System.out.println(prueba.vector.toString());
//    }
    
}
