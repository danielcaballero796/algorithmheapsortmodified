/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ufps.util.colecciones_seed.Secuencia;
import model.Sort;

/**
 *
 * @author danis
 */
public class Main {
    
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        int n = 20;
        Secuencia<Integer> x = new Secuencia<Integer>(n);
        for (int i = 0; i < n; i++) {
            x.insertar(i);
        }
        
       
        Sort prueba = new Sort(x);
        prueba.heapSort();
        System.out.println(prueba.getVector().toString());
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin-inicio) / 1000);

        System.out.println(tiempo + " segundos");
    }
    
}
