/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author danis
 */
public class Sort2 {
    
    /**
     * @param args the command line arguments
     */
    public int[] minHeap(int A[], int n) {
        int cab;
        for (int i = 0; i < n; i++) {
            int x = i;
            do {
                cab = (x - 1) / 2;
                if (A[cab] > A[x]) {
                    int temp = A[cab];
                    A[cab] = A[x];
                    A[x] = temp;
                }
                x = cab;
            } while (x != 0);
        }
        return A;
    }

    public int[] heapSort(int A[], int tam) {
        int x = tam - 1;

        if (tam >= 5) {

            swap(A, 0, x);
            if (A[1] < A[2]) {
                swap(A, 1, x - 1);
            } else {
                swap(A, 2, x - 1);
            }
            minHeap(A, tam - 2);
        } else {
            swap(A, 0, x);
            minHeap(A, tam - 1);
        }
        return A;
    }

    public void swap(int A[], int dad, int son) {
        int temp = A[dad];
        A[dad] = A[son];
        A[son] = temp;
    }

    public void print(int A[]) {
        for (int i = 0; i < A.length; i++) {
            System.out.print("|" + A[i]);
        }
        System.out.println("\n");
    }

    public static void main(String[] args) {

        Sort2 d = new Sort2();

        int arreglo[] = new int[7];
        arreglo[0] = 34;
        arreglo[1] = 62;
        arreglo[2] = 72;
        arreglo[3] = 84;
        arreglo[4] = 92;
        arreglo[5] = 10;
        arreglo[6] = 7;

        int[] a = d.minHeap(arreglo, arreglo.length);
        d.print(a);
        int[] b = d.heapSort(a, a.length);
        d.print(b);

    }

    
}
