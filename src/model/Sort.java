/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author Jaimes Rodriguez
 */
public class Sort<T extends Comparable<T>> {

    private Secuencia<T> vector;
    
    public Sort() {
   }

    public Sort(Secuencia<T> vtc) {
        this.vector = new Secuencia(vtc.getTamanio() + 1);

        for (int i = 0; i < vtc.getTamanio(); i++) {
            this.add(vtc.get(i));
        }
    }

    public void add(T info) {
        this.vector.insertar(info);
        int tam = this.vector.getTamanio();
        siftUp(tam);

    }

    //basado en un Min-Heap
    public void siftUp(int tam) {
        int cab;
        for (int i = 0; i < tam; i++) {
            int x = i;
            while (x != 0) {
                cab = (x - 1) / 2;
                if (this.vector.get(cab).compareTo(this.vector.get(x)) >= 0) {
                    swap(cab, x);
                }
                x = cab;
            }
        }
    }

    public void swap(int dad, int son) {
        T temp = this.vector.get(dad);
        this.vector.set(dad, this.vector.get(son));
        this.vector.set(son, temp);
    }
    
    public void siftDown(int tam) {
        int x = tam - 1;

        if (tam >= 5) {
            swap(0, x);
            if (this.vector.get(1).compareTo(this.vector.get(2)) < 0) {
                swap(1, x - 1);
            } else {
                swap(2, x - 1);
            }
            siftUp(tam - 2);
            
        } else {
            swap(0, x);
            siftUp(tam - 1);
        }
    }

    public void heapSort() {
        int tam = this.vector.getTamanio();
        siftDown(tam);
    }

    public Secuencia<T> getVector() {
        return vector;
    }

}
